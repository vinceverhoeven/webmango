<?php

namespace Webmango\ProductManual\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webmango\ProductManual\Model\ProductManualUpload;
use \Magento\Framework\App\RequestInterface;

class AfterProductSave implements ObserverInterface
{
    protected $request;

    protected $productManualUpload;

    public function __construct(
        ProductManualUpload $productManualUpload,
        RequestInterface $request
    )
    {
        $this->productManualUpload = $productManualUpload;
        $this->request = $request;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $product = $observer->getProduct();

        if (!empty($product)) {
            $storeId = $this->request->getParam('store', 0);
            $productId = $product->getId();

            $productManuals = $this->request->getParam('product_manual');
            if (isset($productManuals) && isset($productId)) {
                foreach ($productManuals as $productManual) {
                    $this->productManualUpload->saveProductManual($storeId, $productId,
                        $productManual["title"], $productManual["file"]);
                }
            }
        }
    }
}
