<?php

namespace Webmango\ProductManual\Block\Adminhtml\Edit;

use Webmango\ProductManual\Model\ResourceModel\Collection\ProductManualCollection;
use \Magento\Framework\UrlInterface;
use \Magento\Backend\Block\Template\Context;
use \Magento\Framework\Registry;
use \Magento\Store\Model\StoreManagerInterface;
use \Webmango\ProductManual\Model\ProductManualUpload;

class ProductManual extends \Magento\Backend\Block\Template
{

    const MANUAL_UPLOAD_URL = "product_manual/manual/upload";

    const MANUAL_REMOVE_URL = "product_manual/manual/remove";

    protected $_storeManager;

    protected $urlBuilder;

    protected $_coreRegistry;

    protected $productManualCollection;

    protected $productManualUpload;


    public function __construct(
        ProductManualUpload $productManualUpload,
        StoreManagerInterface $_storeManager,
        ProductManualCollection $productManualCollection,
        UrlInterface $urlBuilder,
        Context $context,
        Registry $registry,
        array $data = []
    )
    {
        $this->productManualUpload = $productManualUpload;
        $this->_storeManager = $_storeManager;
        $this->productManualCollection = $productManualCollection;
        $this->_coreRegistry = $registry;
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $data);
    }


    /**
     * Retrieve current manuals of product by store id
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return array
     */
    public function getManuals($product)
    {
        if (!$product) {
            $product = $this->getProduct();
        }

        return $this->productManualCollection->getManualsByStore(
            $product, $this->getStoreId()
        );
    }

    /**
     * Retrieve currently viewed product object
     *
     * @return \Magento\Catalog\Model\Product | null
     */
    public function getProduct()
    {
        if (!$this->hasData('product')) {
            $this->setData('product', $this->_coreRegistry->registry('product'));
        }
        return $this->getData('product');
    }

    /**
     * Retrieve manual upload url
     *
     * @return string
     */
    public function getManualUploadUrl()
    {
        return $this->getUrl(self::MANUAL_UPLOAD_URL);
    }

    /**
     * Retrieve manual remove url
     *
     * @return string
     */
    public function getManualRemoveUrl()
    {
        return $this->getUrl(self::MANUAL_REMOVE_URL);
    }

    /**
     * Retrieve manual public url
     *
     * @param string $filename
     * @return string
     */
    public function getManualUploadLocation($filename = "")
    {
        return $this->productManualUpload->getManualFileUrl($filename);
    }

    /**
     * Get store identifier
     *
     * @return  int
     */
    public function getStoreId()
    {
        return $this->_storeManager->getStore()->getId();
    }
}
