<?php

namespace Webmango\ProductManual\Model;

class ProductManual extends \Magento\Framework\Model\AbstractModel implements ProductManualInterface
{
    protected function _construct()
    {
        $this->_init('Webmango\ProductManual\Model\ResourceModel\ProductManualResource');
    }
}
