<?php

namespace Webmango\ProductManual\Model;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\UrlInterface;
use Webmango\ProductManual\Model\ProductManualFactory;
use Webmango\ProductManual\Model\ResourceModel\ProductManualResource;
use Webmango\ProductManual\Model\ResourceModel\Collection\ProductManualCollection;

class ProductManualUpload
{

    const UPLOAD_DIR = "webmango_product_manual";

    const UPLOAD_DIR_TMP = "webmango_product_manual/tmp";

    /**
     * @var UrlInterface
     */
    protected $_urlBuilder;

    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $uploaderFactory;

    /**
     * @var \Magento\Framework\Filesystem\Directory\Write
     */
    protected $mediaDirectoryWrite;

    /**
     * @var \Webmango\ProductManual\Model\ProductManualFactory
     */
    protected $productManualFactory;

    /**
     * @var \Webmango\ProductManual\Model\ResourceModel\ProductManualResource
     */
    protected $productManualResource;

    /**
     * @var ProductManualCollection;
     */
    protected $productManualCollection;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    public function __construct(
        ProductManualCollection $productManualCollection,
        ProductManualFactory $productManualFactory,
        ProductManualResource $productManualResource,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory,
        \Psr\Log\LoggerInterface $logger,
        UrlInterface $urlBuilder
    )
    {
        $this->productManualCollection = $productManualCollection;
        $this->uploaderFactory = $uploaderFactory;
        $this->mediaDirectoryWrite = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->productManualFactory = $productManualFactory;
        $this->productManualResource = $productManualResource;
        $this->_urlBuilder = $urlBuilder;
        $this->logger = $logger;
    }

    public function productManualFileTmp()
    {
        if (!$this->mediaDirectoryWrite->isWritable(self::UPLOAD_DIR_TMP)) {
            $this->mediaDirectoryWrite->create(self::UPLOAD_DIR_TMP);
        }

        $targetPath = $this->mediaDirectoryWrite->getAbsolutePath(self::UPLOAD_DIR_TMP);
        $uploader = $this->uploaderFactory->create([
            'fileId' => 'file'
        ]);
        $uploader->setAllowedExtensions(['gif', 'png', 'jpg', 'jpeg', 'pdf']);
        $uploader->setAllowRenameFiles(true);
        $result = $uploader->save($targetPath);

        $result['file'] = ltrim($result['file'], '/');
        $result['url'] = $this->_urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA])
            . self::UPLOAD_DIR_TMP . '/'
            . $result['file'];


        return $result;
    }

    public function moveProductManualFileTmp($fileName)
    {
        try {
            $this->mediaDirectoryWrite->renameFile(
                self::UPLOAD_DIR_TMP . '/' . $fileName,
                self::UPLOAD_DIR . '/' . $fileName
            );
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }
    }

    /**
     * Retrieve manual public url
     *
     * @param string $filename
     * @return string
     */
    public function getManualFileUrl($filename = "")
    {
        return $this->_urlBuilder->getBaseUrl() . 'pub/media/' .
            self::UPLOAD_DIR . '/' . $filename;
    }

    public function saveProductManual($storeId, $productId, $fileTitle, $fileName, $manualId = false)
    {
        /**
         * @var $productManual \Webmango\ProductManual\Model\ProductManual
         */
        //  $productManual = $this->getManualById($manualId);

        $productManual = $this->productManualFactory->create();

        $productManual->setData(array(
            "title" => $fileTitle,
            "file_name" => $fileName,
            "store_id" => $storeId,
            "product_id" => $productId,
        ));
        $this->productManualResource->save($productManual);
        $this->moveProductManualFileTmp($fileName);
    }

    public function getManualById($id)
    {
        $manual = $this->productManualCollection->addFieldToFilter('id', $id)->getFirstItem();
        if ($manual) {
            return $manual;
        }
        return false;
    }


    public function deleteFile($name)
    {
        $targetPath = $this->mediaDirectoryWrite->getAbsolutePath(self::UPLOAD_DIR) . '/' . $name;
        $this->mediaDirectoryWrite->delete($targetPath);
    }

}
