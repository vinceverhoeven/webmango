<?php

namespace Webmango\ProductManual\Model\ResourceModel\Collection;

class ProductManualCollection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Webmango\ProductManual\Model\ProductManual', 'Webmango\ProductManual\Model\ResourceModel\ProductManualResource');
    }

    /**
     * Retrieve current manuals of product by store id
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param int | string $storeId
     * @return array
     */
    public function getManualsByStore($product, $storeId)
    {
        $manuals = array();

        if ($product) {
            $collection = $this->addFieldToFilter('store_id', array('in' => array(0, $storeId)))
                ->addFieldToFilter('product_id', $product->getId())->load();

            if ($collection) {
                $manuals = $collection->getItems();
            }
        }
        return $manuals;
    }

    /**
     * Retrieve current manuals of product by product
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return array
     */
    public function getManualsByProduct($product)
    {
        $manuals = array();

        $collection = $this->addFieldToFilter('product_id', $product->getId())->load();
        if ($collection) {
            $manuals = $collection->getItems();
        }
        return $manuals;
    }
}

