<?php

namespace Webmango\ProductManual\Controller\Adminhtml\Manual;

use Magento\Framework\App\Filesystem\DirectoryList;
use Webmango\ProductManual\Model\ProductManualUpload;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\Controller\Result\JsonFactory;
use \Magento\Framework\Filesystem;
use \Magento\MediaStorage\Model\File\UploaderFactory;

class Upload extends \Magento\Backend\App\Action
{

    const UPLOAD_DIR = "webmango_product_manual";

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $uploaderFactory;

    /**
     * @var \Magento\Framework\Filesystem\Directory\Write
     */
    protected $mediaDirectoryWrite;

    protected $productManualUpload;

    public function __construct(
        ProductManualUpload $productManualUpload,
        Context $context,
        JsonFactory $resultJsonFactory,
        Filesystem $filesystem,
        UploaderFactory $uploaderFactory
    )
    {
        $this->productManualUpload = $productManualUpload;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->uploaderFactory = $uploaderFactory;
        $this->mediaDirectoryWrite = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        try {
            $result = $this->productManualUpload->productManualFileTmp();
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }

        return $resultJson->setData($result);
    }
}
