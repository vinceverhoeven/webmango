<?php

namespace Webmango\ProductManual\Controller\Adminhtml\Manual;

use Webmango\ProductManual\Model\ProductManualUpload;
use Webmango\ProductManual\Model\ResourceModel\ProductManualResource;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\Controller\Result\JsonFactory;
use \Magento\MediaStorage\Model\File\UploaderFactory;
use Webmango\ProductManual\Model\ProductManualFactory;

class Remove extends \Magento\Backend\App\Action
{

    protected $resultJsonFactory;

    protected $productManualUpload;

    protected $productManualResource;

    protected $productManualFactory;

    public function __construct(
        ProductManualUpload $productManualUpload,
        Context $context,
        JsonFactory $resultJsonFactory,
        ProductManualFactory $productManualFactory,
        ProductManualResource $productManualResource
    )
    {
        $this->productManualFactory = $productManualFactory;
        $this->productManualResource = $productManualResource;
        $this->productManualUpload = $productManualUpload;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        $result = array();
        try {
            $id = $this->_request->getParam('manual_id');
            $model = $this->productManualFactory->create()->load($id);
            $this->productManualUpload->deleteFile($model->getFileName());
            $this->productManualResource->delete($model);
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }
        return $resultJson->setData($result);
    }
}
