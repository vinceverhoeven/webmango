<?php

namespace Webmango\ProductManual\Plugin\Catalog\Product;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\ProductRepository;
use Webmango\ProductManual\Model\ProductManualUpload;
use Webmango\ProductManual\Model\ResourceModel\Collection\ProductManualCollection;
use Webmango\ProductManual\Model\ResourceModel\ProductManualResource;

class ProductRepositoryPlugin
{
    protected $productManualCollection;

    protected $productManualFactory;

    protected $productManualUpload;

    protected $productManualResource;

    public function __construct(
        ProductManualResource $productManualResource,
        ProductManualCollection $productManualCollection,
        ProductManualUpload $productManualUpload,
        ProductManualFactory $productManualFactory
    )
    {
        $this->productManualResource = $productManualResource;
        $this->productManualCollection = $productManualCollection;
        $this->productManualFactory = $productManualFactory;
        $this->productManualUpload = $productManualUpload;
    }

    public function afterDelete(ProductRepository $subject, $result, ProductInterface $product)
    {
        try {
            $manuals = $this->productManualCollection->getManualsByProduct($product);
            foreach ($manuals as $manual) {
                $this->productManualResource->delete($manual->getId());
                $this->productManualUpload->deleteFile($manual->getFileName());
            }
        } catch (\Exception $e) {

        }
        return $result;
    }

}
