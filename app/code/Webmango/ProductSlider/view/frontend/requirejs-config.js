var config = {
    paths: {
        'owlcarousel': "Webmango_ProductSlider/js/vendor/owl.carousel.min",
        'webmangoProductSlider':"Webmango_ProductSlider/js/productSlider"
    },
    shim: {
        'owlcarousel': {
            deps: ['jquery']
        }
    }
};
