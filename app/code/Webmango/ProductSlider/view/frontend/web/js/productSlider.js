define([
        'jquery',
        'owlcarousel'
    ],
    function ($) {
        $.widget('webmango.productSlider', {
            /**
             * Creates widget
             * @private
             */
            _create: function () {
                this._initializeOptions();
            },
            _initializeOptions: function () {
                var owl = $(this.element).owlCarousel({
                    autoplayTimeout: this.options.autoplayTimeout || 5000,
                    autoplay: true,
                    loop: true,
                    margin: 10,
                    smartSpeed: this.options.smartSpeed || 2000,
                    autoplayHoverPause: true,
                    dots: false,
                });

                if (this.options.maxItems == 1) {
                    owl.data('owl.carousel').options.animateOut = 'animate__fadeOutDown';
                    owl.data('owl.carousel').options.animateIn = 'animate__fadeInUp';
                    owl.data('owl.carousel').options.items = 1;
                } else {
                    owl.data('owl.carousel').options.responsive = {
                        0: {
                            items: 1.2
                        },
                        600: {
                            items: 2
                        },
                        1000: {
                            items: 3
                        }
                    }
                }
                owl.trigger('refresh.owl.carousel');

                $(this.options.nextClass).on('click', function () {
                    owl.trigger('next.owl.carousel');
                });
                $(this.options.prevClass).on('click', function () {
                    owl.trigger('prev.owl.carousel');
                });
            },
        });
        return $.webmango.productSlider;
    }
);
