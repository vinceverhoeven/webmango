<?php
/**
 * Copyright Webmango
 * Author Vince Verhoeven
 * Date: 12 Mar 2019
 */
namespace Webmango\CustomMenu\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class Data
 * @package Webmango\CustomMenu\Helper
 */
class Data extends AbstractHelper
{
    protected $pages_before = array(
    );

    protected $pages_after = array(
        "Blog" => array(
            "title" => "Blog",
            "route" => "blog",
            "btn-class" => ""
        ),
        "Vraag module aan" => array(
            "title" => "Vraag module aan",
            "route" => "contact",
            "btn-class" => ""
        ),
    );

    public function checkActivePage($current, $pageUrl)
    {
        $keys = parse_url($current); // parse the url
        $path = explode("/", $keys['path']); // splitting the path
        $last = end($path); // get the value of the last element
        return (strpos($last, $pageUrl) !== false) ? 'active' : '';
    }

    /**
     * @return array
     */
    public function getPagesBefore()
    {
        return $this->pages_before;
    }

    /**
     * @return array
     */
    public function getPagesAfter()
    {
        return $this->pages_after;
    }
}
