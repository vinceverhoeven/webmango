<?php
/**
 * Copyright Webmango
 * Author Vince Verhoeven
 * Date: 12 Mar 2019
 */

namespace Webmango\CustomMenu\Plugin;

class Topmenu
{
    protected $_session;

    protected $_helper;

    /**
     * Topmenu constructor.
     * @param \Magento\Customer\Model\Session $session
     * @param \Webmango\CustomMenu\Helper\Data $helper
     */
    public function __construct(
        \Magento\Customer\Model\Session $session,
        \Webmango\CustomMenu\Helper\Data $helper
    )
    {
        $this->_session = $session;
        $this->_helper = $helper;
    }

    public function afterGetHtml(\Magento\Theme\Block\Html\Topmenu $topmenu, $html)
    {
        $currentUrl = $topmenu->getUrl('*/*/*', ['_current' => true, '_use_rewrite' => true]);
        $beforeHtml = "";
        $afterHtml = "";
        // before pages
        foreach ($this->_helper->getPagesBefore() as $pages) {
            $url = $topmenu->getUrl($pages["route"]);//here you can set link
            $beforeHtml .= '<li class="level0 nav-4 level-top  ui-menu-item ' . $this->_helper->checkActivePage($currentUrl, $url) . '">';
            $beforeHtml .= "<a href=\"" . $url . "\" class=\"level-top ui-corner-all\"></span><span>" . $pages["title"] . "</span></a>";
            $beforeHtml .= "</li>";
        }
        // after
        foreach ($this->_helper->getPagesAfter() as $pages) {
            $url = $topmenu->getUrl($pages["route"]);//here you can set link
            $afterHtml .= '<li class="level0 nav-4 level-top  ui-menu-item ' . $this->_helper->checkActivePage($currentUrl, $url) . '">';
            $afterHtml .= '<a href="'. $url . '" class="level-top ui-corner-all '.$pages['btn-class'].'"></span><span>' . $pages["title"] . '</span></a>';
            $afterHtml .= "</li>";
        }
        return $beforeHtml . $html . $afterHtml;
    }
}
