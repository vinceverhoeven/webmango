<?php
/**
 * 11-03-20
 * Author Vince Verhoeven
 */

namespace Webmango\Snippets\Block;

use Magento\Cms\Block\Block;

/**
 * Cms block content block
 */
class SnippetBlock extends Block
{

    protected function _toHtml()
    {
        $html = '';

        $blockId = $this->getBlockId();
        $block = $this->getSnippetBlock($blockId);

        if ($block) {
            $html = $this->getSnippetHtml($block);
        }
        return $html;
    }

    public function getSnippetBlock($id = '')
    {
        $block = false;

        if (empty($id))
            $id = $this->getBlockId();

        if ($id) {
            $validBlock = $this->_blockFactory->create();
            $validBlock->setStoreId($this->getStoreId())->load($id);
            if ($validBlock->isActive()) {
                $block = $validBlock;
            }
        }
        return $block;
    }

    protected function getStoreId()
    {
        return $this->_storeManager->getStore()->getId();
    }

    protected function getSnippetHtml($block)
    {
        $html = '';
        $content = $this->getSnippetBlockContent($block);
        $imageContent = '';

        if (!empty($content)) {
            if ($this->hasContentClass())
                $content = '<div class="snippet-content ' . $this->getContentClass() . '">' . $content . '</div>';
        }

        $html = $content;

        if ($this->hasShowImage() && $this->getShowImage()) {
            $imageUrl = $this->getSnippetBlockImageUrl($block);
            if ($imageUrl) {
                $imageContent = '<img src="' . $imageUrl . '"/>';
                if ($this->hasImageClass())
                    $imageContent = '<div class="snippet-image ' . $this->getImageClass() . '">' . $imageContent . '</div>';
            }

            $html =  $imageContent . $content;

            if ($this->hasImageFirst() && !$this->getImageFirst())
                $html = $content . $imageContent;

        }

        return $html;
    }

    public function getSnippetBlockContent($block)
    {
        return $this->_filterProvider->getBlockFilter()->setStoreId($this->getStoreId())->filter($block->getContent());
    }

    public function getSnippetBlockImageUrl($block)
    {
        $imageUrl = false;
        $imageName = $block->getImage();
        if ($imageName) {
            $imageUrl = '/media/Webmango_Snippets/' . $block->getImage();
        }
        return $imageUrl;
    }

}
