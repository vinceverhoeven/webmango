<?php

namespace Webmango\Snippets\Plugin;

use Webmango\Snippets\Helper\Data as SnippetHelper;
use \Magento\Framework\App\Response\RedirectInterface;

Class OnInsert
{
    protected $response;

    protected $_snippetHelper;

    public function __construct(
        SnippetHelper $snippetHelper,
        RedirectInterface $response
    )
    {
        $this->response = $response;
        $this->_snippetHelper = $snippetHelper;
    }


    public function afterExecute(\Magento\Cms\Controller\Adminhtml\Wysiwyg\Images\OnInsert $subject, $result)
    {
        // only execute on block adminhtml page
        if (strpos($this->response->getRefererUrl(), 'block_id') !== false) {
            $this->_snippetHelper->setNewImageSession(true);
        }
        return $result;
    }
}
