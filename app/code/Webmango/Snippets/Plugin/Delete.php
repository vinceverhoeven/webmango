<?php

namespace Webmango\Snippets\Plugin;

use Magento\Framework\App\RequestInterface;
use Webmango\Snippets\Model\ImageUploader;
use \Magento\Cms\Model\BlockRepository;

Class Delete
{
    protected $imageUploader;

    protected $blockRepository;

    protected $request;

    public function __construct(
        RequestInterface $request,
        BlockRepository $blockRepository,
        ImageUploader $imageUploader
    )
    {
        $this->request = $request;
        $this->blockRepository = $blockRepository;
        $this->imageUploader = $imageUploader;
    }

    /**
     *
     * @param \Magento\Cms\Controller\Adminhtml\Block\Delete $subject
     * @param $proceed
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function beforeExecute(\Magento\Cms\Controller\Adminhtml\Block\Delete $subject)
    {
        $id = $this->request->getParam('block_id');
        if ($id) {
            try {
                $block = $this->blockRepository->getById($id);
                $blockImageName = $block->getImage();
                $this->imageUploader->deleteFile($blockImageName);
            } catch (\Exception $e) {
            }
        }
    }
}
