<?php
/**
* 11-03-20
* Author Vince Verhoeven
*/

use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Webmango_Snippets', __DIR__);
