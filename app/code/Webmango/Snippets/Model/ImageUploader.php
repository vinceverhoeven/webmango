<?php
/**
 * 11-03-20
 * Author Vince Verhoeven
 */

namespace Webmango\Snippets\Model;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\File\Uploader;
use Magento\Framework\UrlInterface;

/**
 * Catalog image uploader
 */
class ImageUploader
{
    protected $coreFileStorageDatabase;
    protected $mediaDirectory;
    private $uploaderFactory;
    protected $storeManager;
    protected $logger;
    const BASE_TMP_PATH = 'Webmango_Snippets/tmp/';
    const BASE_PATH = 'Webmango_Snippets/';
    protected $allowedExtensions;
    private $allowedMimeTypes;
    protected $filesystem;

    public function __construct(
        \Magento\MediaStorage\Helper\File\Storage\Database $coreFileStorageDatabase,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Psr\Log\LoggerInterface $logger,
        $allowedExtensions = ['jpg', 'jpeg', 'gif', 'png'],
        $allowedMimeTypes = []
    )
    {
        $this->filesystem = $filesystem;
        $this->coreFileStorageDatabase = $coreFileStorageDatabase;
        $this->mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->uploaderFactory = $uploaderFactory;
        $this->storeManager = $storeManager;
        $this->logger = $logger;
        $this->allowedExtensions = $allowedExtensions;
        $this->allowedMimeTypes = $allowedMimeTypes;
    }

    public function setAllowedExtensions($allowedExtensions)
    {
        $this->allowedExtensions = $allowedExtensions;
    }

    public function getAllowedExtensions()
    {
        return $this->allowedExtensions;
    }

    public function getFilePath($path, $imageName)
    {
        return rtrim($path, '/') . '/' . ltrim($imageName, '/');
    }

    public function getNewFileName($filename)
    {
        // only generate when there is already same filename inside base upload dir
        if ($this->checkFileExist($this->getBasePath(), $filename)) {
            $filename = Uploader::getNewFileName(
                $this->mediaDirectory->getAbsolutePath(
                    $this->getFilePath($this->getBasePath(), $filename)
                )
            );
        }
        return $filename;
    }


    public function moveFileFromTmp($relativeUrl, $oldImageName, $imageName, $move)
    {
        if (!$this->checkFileExist($relativeUrl, $oldImageName))
            return;

        $baseImagePath = $this->getBasePath() . $imageName;

        $path = $relativeUrl . $oldImageName;

        try {
            if ($move) {
                $this->mediaDirectory->renameFile(
                    $path,
                    $baseImagePath
                );
            } else {
                $this->mediaDirectory->copyFile(
                    $path,
                    $baseImagePath
                );
            }
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Something went wrong while saving the file(s).')
            );
        }

        return $imageName;
    }


    public function checkFileExist($relativeUrl, $imageName)
    {
        $path = $relativeUrl . $imageName;
        return $this->mediaDirectory->isExist($this->filesystem->getDirectoryRead(
            DirectoryList::MEDIA)->getAbsolutePath($path));
    }

    public function deleteFile($fileName)
    {
        if ($fileName)
            $this->mediaDirectory->delete($this->getAbsolutePath($fileName));
    }

    public function saveFileToTmpDir($fileId)
    {
        $baseTmpPath = $this->getBaseTmpPath();
        /** @var \Magento\MediaStorage\Model\File\Uploader $uploader */
        $uploader = $this->uploaderFactory->create(['fileId' => $fileId]);

        $file = $uploader->validateFile();
        $uniqueFileName = $this->getNewFileName($file["name"]);

        $uploader->setAllowedExtensions($this->getAllowedExtensions());
        $uploader->setAllowRenameFiles(true);
        if (!$uploader->checkMimeType($this->allowedMimeTypes)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('File validation failed.'));
        }
        $result = $uploader->save($this->mediaDirectory->getAbsolutePath($baseTmpPath), $uniqueFileName);
        unset($result['path']);

        if (!$result) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('File can not be saved to the destination folder.')
            );
        }

        //  Workaround for prototype 1.7 methods "isJSON", "evalJSON" on Windows OS
        $result['tmp_name'] = str_replace('\\', '/', $result['tmp_name']);
        $result['url'] = '/' . UrlInterface::URL_TYPE_MEDIA . '/' . $this->getBaseTmpPath() . $uniqueFileName;
        $result['name'] = $result['file'];
        $result['not_from_gallery'] = true;
        $result['dwd_new_image'] = true;

        if (isset($result['file'])) {
            try {
                $relativePath = rtrim($baseTmpPath, '/') . '/' . ltrim($result['file'], '/');
                $this->coreFileStorageDatabase->saveFile($relativePath);
            } catch (\Exception $e) {
                $this->logger->critical($e);
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Something went wrong while saving the file(s).')
                );
            }
        }

        return $result;
    }

    protected function prepareFile($file)
    {
        return ltrim(str_replace('\\', '/', $file), '/');
    }

    public function getImageUrl($name = '')
    {
        $mediaPath = $this->storeManager->getStore()->getBaseUrl(
            \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
        );
        $mediaPath .= $this->getFilePath($this->getBasePath(), $name);
        return $mediaPath;
    }


    protected function getAbsolutePath($name = '')
    {
        return $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath(
            $this->getFilePath($this->getBasePath(), $name));
    }


    public function getBasePath()
    {
        return self::BASE_PATH;
    }

    public function getBaseTmpPath()
    {
        return self::BASE_TMP_PATH;
    }

    /**
     * used for data provider
     */
    public function convertValues($block)
    {
        $imageName = $block->getImage();
        $image = [];
        if ($imageName && $this->checkFileExist($this->getBasePath(), $imageName)) {
            $imageSize = getimagesize($this->getAbsolutePath($imageName));
            $image[0]['name'] = $imageName;
            $image[0]['type'] = 'image';
            $image[0]['url'] = '/media/' . $this->getFilePath($this->getBasePath(), $imageName);
            $image[0]['size'] = filesize($this->getAbsolutePath($imageName));
            $image[0]['width'] = $imageSize ? $imageSize[0] : 0;
            $image[0]['height'] = $imageSize ? $imageSize[1] : 0;
        }
        $block->setImage($image);
        return $block;
    }
}
