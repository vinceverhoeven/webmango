<?php
/**
* 11-03-20
* Author Vince Verhoeven
*/

namespace Webmango\Snippets\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Backend\Model\Session as BackendSession;

class Data extends AbstractHelper
{

    protected $_session;

    public function __construct(
        BackendSession $session
    )
    {
        $this->_session = $session;
    }

    /**
     * @return boolean
     */
    public function getNewImageSession()
    {
        return $this->_session->getNewImage();
    }

    /**
     * @param boolean $value
     */
    public function setNewImageSession($value)
    {
        $this->_session->setNewImage($value);
    }

    public function unsetNewImageSession()
    {
        $this->_session->unsNewImage('');
    }

}
