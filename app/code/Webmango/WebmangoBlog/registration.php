<?php
/**
 * @author: Vince Verhoeven
 * @date: 16-07-20
 *
 *
 *  Todo Create db fields > enabled title content image url-key meta-content store
 *  Todo Create grid
 *  Todo Create edit
 *  Todo Create save
 */

use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Webmango_WebmangoBlog', __DIR__);
