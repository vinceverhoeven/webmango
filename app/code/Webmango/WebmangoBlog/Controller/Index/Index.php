<?php
namespace Webmango\WebmangoBlog\Controller\Index;

use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;
use \Magento\Framework\App\Action\Action;
use Webmango\WebmangoBlog\Model\Post\BlogRepository;
use \Magento\Framework\Api\SearchCriteriaBuilder;


class Index extends Action implements HttpGetActionInterface
{
    protected $searchCriteriaBuilder;

    protected $blogRepository;

    protected $_pageFactory;

    public function __construct(
        SearchCriteriaBuilder $searchCriteriaBuilder,
        BlogRepository $blogRepository,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->blogRepository = $blogRepository;
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $allBlogs = $this->blogRepository->getList($searchCriteria);

        echo '<pre style="position: absolute; z-index: 9999; background-color: white">';
        var_dump($allBlogs->getTotalCount());
        echo '</pre>';
        die;
        return $this->_pageFactory->create();
    }
}
