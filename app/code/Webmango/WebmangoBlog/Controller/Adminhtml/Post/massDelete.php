<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Webmango\WebmangoBlog\Controller\Adminhtml\Post;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Webmango\WebmangoBlog\Model\Post\ResourceModel\Grid\CollectionFactory;
use Webmango\WebmangoBlog\Model\Post\ResourceModel\Blog;
use Webmango\WebmangoBlog\Model\Post\BlogFactory;
use Webmango\WebmangoBlog\Model\UrlRewrite\UrlRewrite;
use Webmango\WebmangoBlog\Controller\Adminhtml\Image\Upload as WebmangoBlogUploader;

/**
 * Class MassDelete
 */
class MassDelete extends \Magento\Backend\App\Action implements HttpPostActionInterface
{

    protected $filter;

    protected $collectionFactory;

    protected $blogResourceModel;

    protected $blogFactory;

    protected $_WebmangoBlogUploader;

    protected $_urlRewrite;

    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        Blog $blogResourceModel,
        BlogFactory $blogFactory,
        WebmangoBlogUploader $WebmangoBlogUploader,
        UrlRewrite $urlRewrite
    )
    {
        $this->_WebmangoBlogUploader = $WebmangoBlogUploader;
        $this->_urlRewrite = $urlRewrite;
        $this->blogFactory = $blogFactory;
        $this->blogResourceModel = $blogResourceModel;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $collectionSize = $collection->getSize();

        foreach ($collection as $item) {
            $blog = $this->blogFactory->create()->load($item->getId());
            $this->_urlRewrite->deleteBlogRewrite($item->getId());
            $this->_WebmangoBlogUploader->removeBlogThumbnail($item->getThumbnail());
            $blog->delete();
        }

        $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been deleted.', $collectionSize));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}
