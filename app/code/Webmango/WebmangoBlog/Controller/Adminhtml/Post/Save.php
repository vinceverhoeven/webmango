<?php

namespace Webmango\WebmangoBlog\Controller\Adminhtml\Post;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Backend\App\Action;
use Webmango\WebmangoBlog\Model\Post\Blog;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Webmango\WebmangoBlog\Helper\Helper as WebmangoHelper;
use \Webmango\WebmangoBlog\Controller\Post\View as BlogViewController;
use Webmango\WebmangoBlog\Model\UrlRewrite\UrlRewrite;
use \Webmango\WebmangoBlog\Model\Post\ResourceModel\Blog as BlogResource;
/**
 * Save CMS page action.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Save extends \Magento\Backend\App\Action implements HttpPostActionInterface
{

    protected $blogResource;

    protected $webmangoHelper;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var \Webmango\WebmangoBlog\Model\Post\BlogFactory
     */
    private $blogFactory;


    protected $_urlRewrite;

    public function __construct(
        Action\Context $context,
        DataPersistorInterface $dataPersistor,
        WebmangoHelper $webmangoHelper,
        UrlRewrite $urlRewrite,
        \Webmango\WebmangoBlog\Model\Post\BlogFactory $blogFactory = null,
        BlogResource $blogResource
    )
    {
        $this->blogResource = $blogResource;
        $this->_urlRewrite = $urlRewrite;
        $this->webmangoHelper = $webmangoHelper;
        $this->dataPersistor = $dataPersistor;
        $this->blogFactory = $blogFactory ?: ObjectManager::getInstance()->get(\Webmango\WebmangoBlog\Model\Post\BlogFactory::class);
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            if (isset($data['is_active']) && $data['is_active'] === 'true') {
                $data['is_active'] = Blog::STATUS_ENABLED;
            }

            if (empty($data['blog_id'])) {
                $data['blog_id'] = null;
            }

            if (isset($data["store_id"])) {
                $data["store_id"] = $data["store_id"][0];
            }

            if (empty($data['url_key'])) {
                // todo not save url if all storeview url exist
                $data['url_key'] = $this->_urlRewrite->createUrlKey($data['blog_id'], $data["title"], $data["store_id"]);
            }else{
                $data['url_key'] = $this->_urlRewrite->createUrlKey($data['blog_id'], $data["url_key"], $data["store_id"]);
            }

            if (isset($data["thumbnail"])) {
                $data["thumbnail"] = $data["thumbnail"][0]['name'];
            }

            /**
             *  @var \Webmango\WebmangoBlog\Model\Post\Blog $model
             */
            $model = $this->blogFactory->create();
            $id = $this->getRequest()->getParam('blog_id');
            if ($id) {
                try {
                   // $this->blogResource->load($model,$id);
                    $model = $model->load($id);
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('This page or the url no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }
            $model->setData($data);
            try {
                $this->_eventManager->dispatch(
                    'webmango_blog_prepare_save',
                    ['blog' => $model, 'request' => $this->getRequest()]
                );
                $model->save();
                $blogId = $model->getId();

                // blog already exist
                if ($id) {
                    $blogId = $id;
                }

                try {
                    $this->_urlRewrite->saveBlogRewrite($blogId,  $data['url_key'], $data['store_id']);
                } catch (LocalizedException $e) {
                    $this->messageManager->addExceptionMessage($e, $e->getMessage());
                }

                $this->messageManager->addSuccessMessage(__('You saved the blog.'));
                return $this->processResultRedirect($model, $resultRedirect, $data);
            } catch (LocalizedException $e) {
                $this->messageManager->addExceptionMessage($e->getPrevious() ?: $e);
            } catch (\Throwable $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the blog.'));
            }

            $this->dataPersistor->set('webmango_blog', $data);
            return $resultRedirect->setPath('*/*/edit', ['blog_id' => $this->getRequest()->getParam('blog_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Process result redirect
     *
     * @param \Webmango\WebmangoBlog\Model\Post\BlogFactory|null $blogFactory
     * @param \Magento\Backend\Model\View\Result\Redirect $resultRedirect
     * @param array $data
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws LocalizedException
     */
    private function processResultRedirect($model, $resultRedirect, $data)
    {
        if ($this->getRequest()->getParam('back', false) === 'duplicate') {
            $newBlog = $this->blogFactory->create(['data' => $data]);
            $newBlog->setId(null);
            $identifier = $model->getIdentifier() . '-' . uniqid();
            $newBlog->setIdentifier($identifier);
            $newBlog->setIsActive(false);
            $newBlog->save($newBlog);
            $this->messageManager->addSuccessMessage(__('You duplicated the blog.'));
            return $resultRedirect->setPath(
                '*/*/edit',
                [
                    'blog_id' => $newBlog->getId(),
                    '_current' => true
                ]
            );
        }
        $this->dataPersistor->clear('webmango_blog');
        if ($this->getRequest()->getParam('back')) {
            return $resultRedirect->setPath('*/*/edit', ['blog_id' => $model->getId(), '_current' => true]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
