<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Webmango\WebmangoBlog\Controller\Adminhtml\Post;

use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Webmango\WebmangoBlog\Model\Post\BlogFactory;
use Webmango\WebmangoBlog\Controller\Adminhtml\Image\Upload as WebmangoBlogUploader;
use Webmango\WebmangoBlog\Model\UrlRewrite\UrlRewrite;

/**
 * Delete Blog post action.
 */
class Delete extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    protected $_blogFactory;

    protected $_WebmangoBlogUploader;

    protected $_urlRewrite;

    public function __construct(
        Action\Context $context,
        BlogFactory $blogFactory,
        WebmangoBlogUploader $WebmangoBlogUploader,
        UrlRewrite $urlRewrite
    )
    {
        $this->_urlRewrite = $urlRewrite;
        $this->_blogFactory = $blogFactory;
        $this->_WebmangoBlogUploader = $WebmangoBlogUploader;
        parent::__construct($context);
    }


    /**
     * Delete action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public
    function execute()
    {
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('blog_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($id) {
            $title = "";
            try {
                // init model and delete
                $model = $this->_blogFactory->create()->load($id);

                $title = $model->getTitle();
                $this->_urlRewrite->deleteBlogRewrite($id);

                $this->_WebmangoBlogUploader->removeBlogThumbnail($model->getThumbnail());
                $model->delete();

                // display success message
                $this->messageManager->addSuccessMessage(__('The blog has been deleted.'));

                // go to grid
                $this->_eventManager->dispatch('webmango_blog_on_delete', [
                    'title' => $title,
                    'status' => 'success'
                ]);

                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->_eventManager->dispatch(
                    'adminhtml_cmspage_on_delete',
                    ['title' => $title, 'status' => 'fail']
                );
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['blog_id' => $id]);
            }
        }

        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a blog post to delete.'));

        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
