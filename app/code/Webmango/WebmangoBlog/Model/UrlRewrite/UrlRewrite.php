<?php

namespace Webmango\WebmangoBlog\Model\UrlRewrite;

use Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollection;
use \Magento\UrlRewrite\Model\UrlRewriteFactory;

class UrlRewrite
{

    const TARGET_PATH = "blog/post/view/blog_id/";

    const ENTITY_TYPE = "blog-post";

    protected $_urlRewriteCollection;

    protected $_urlRewriteFactory;

    public function __construct(
        UrlRewriteFactory $urlRewriteFactory,
        UrlRewriteCollection $urlRewriteCollection
    )
    {
        $this->_urlRewriteFactory = $urlRewriteFactory;
        $this->_urlRewriteCollection = $urlRewriteCollection;
    }

    public function deleteBlogRewrite($entityId)
    {
        $urlRewriteCollection = $this->_urlRewriteCollection->addFieldToFilter('entity_id', $entityId)
            ->addFieldToFilter('entity_type', self::ENTITY_TYPE);
        $item = $urlRewriteCollection->getFirstItem();
        if ($item->getId()) {
            $this->_urlRewriteFactory->create()->load($item->getId())->delete();
        }
        $urlRewriteCollection->clear()->getSelect()->reset(\Zend_Db_Select::WHERE);
    }

    public function loadBlogRewrite($entityId)
    {
        $rewrite = false;

        $urlRewriteCollection = $this->_urlRewriteCollection->addFieldToFilter('entity_id', $entityId)
            ->addFieldToFilter('entity_type', self::ENTITY_TYPE);
        $item = $urlRewriteCollection->getFirstItem();

        if ($item->getId()) {
            $rewrite = $this->_urlRewriteFactory->create()->load($item->getId());
        }
        return $rewrite;
    }

    public function checkUrlKeyDuplicates($entityId, $requestPath, $storeId)
    {
        $isDuplicate = false;
        $urlRewriteCollection = $this->_urlRewriteCollection->addFieldToFilter('entity_id', array('neq' => $entityId))
            ->addFieldToFilter('request_path', $requestPath);
        $urls = $urlRewriteCollection->getItems();

        foreach ($urls as $url) {
            if ($url->getData('store_id') == 0 || $storeId == 0 || $url->getData('store_id') == $storeId ) {
                $isDuplicate = true;
            }
        }
        $urlRewriteCollection->clear()->getSelect()->reset(\Zend_Db_Select::WHERE);

        return $isDuplicate;
    }

    public function saveBlogRewrite($blogId, $url, $storeId = 0)
    {
        $urlRewriteModel = $this->loadBlogRewrite($blogId);

        if (!$urlRewriteModel)
            $urlRewriteModel = $this->_urlRewriteFactory->create();

        $urlRewriteModel->setStoreId($storeId);
        $urlRewriteModel->setIsSystem(0);
        $urlRewriteModel->setEntityId($blogId);
        $urlRewriteModel->setRequestPath($url);
        $urlRewriteModel->setTargetPath(self::TARGET_PATH . $blogId);
        $urlRewriteModel->setEntityType(self::ENTITY_TYPE);
        $urlRewriteModel->save();
    }

    public function createUrlKey($blogId, $title, $storeId)
    {
        $url = preg_replace('#[^0-9a-z]+#i', '-', $title);
        $lastCharTitle = substr($title, -1);
        $lastUrlChar = substr($url, -1);

        if ($lastUrlChar == "-" && $lastCharTitle != "-") {
            $url = substr($url, 0, strlen($url) - 1);
        }

        $urlKey = strtolower($url);
        $hasDuplicate = $this->checkUrlKeyDuplicates($blogId, $urlKey, $storeId);

        if (!$hasDuplicate) {
            return $urlKey;
        } else {
            return $urlKey . '-' . time();
        }
    }

}
