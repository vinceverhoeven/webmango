<?php
namespace Webmango\WebmangoBlog\Model\Post;

class Blog extends \Magento\Framework\Model\AbstractModel implements BlogInterface, \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'webmango_blog';

    protected function _construct()
    {
        $this->_init('Webmango\WebmangoBlog\Model\Post\ResourceModel\Blog');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
