<?php
namespace Webmango\WebmangoBlog\Model\Post\ResourceModel;

class Blog extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('webmango_blog','blog_id');
    }
}
