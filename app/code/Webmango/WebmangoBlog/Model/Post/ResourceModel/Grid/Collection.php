<?php

namespace Webmango\WebmangoBlog\Model\Post\ResourceModel\Grid;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Webmango\WebmangoBlog\Model\Post\Blog','Webmango\WebmangoBlog\Model\Post\ResourceModel\Blog');
    }
}
