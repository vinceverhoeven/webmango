<?php

namespace Webmango\WebmangoBlog\Model\Post;

use Webmango\WebmangoBlog\Api\BlogRepositoryInterface;
use Webmango\WebmangoBlog\Api\Data\BlogInterface;
use Webmango\WebmangoBlog\Api\Data\BlogSearchResultInterfaceFactory;
use Webmango\WebmangoBlog\Model\Post\ResourceModel\Grid\Collection as Collection;
use Webmango\WebmangoBlog\Model\Post\ResourceModel\Grid\CollectionFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\NoSuchEntityException;

class BlogRepository implements BlogRepositoryInterface
{
    /**
     * @var BlogFactory
     */
    private $blogFactory;

    /**
     * @var CollectionFactory
     */
    private $blogCollectionFactory;

    /**
     * @var BlogSearchResultInterfaceFactory
     */
    private $searchResultFactory;

    public function __construct(
        BlogFactory $blogFactory,
        CollectionFactory $blogCollectionFactory,
        BlogSearchResultInterfaceFactory $blogSearchResultInterfaceFactory
    )
    {
        $this->blogFactory = $blogFactory;
        $this->blogCollectionFactory = $blogCollectionFactory;
        $this->searchResultFactory = $blogSearchResultInterfaceFactory;
    }

    public function getById($id)
    {
        $post = $this->blogFactory->create();
        $post->getResource()->load($post, $id);
        if (!$post->getId()) {
            throw new NoSuchEntityException(__('Unable to find blog post with ID "%1"', $id));
        }
        return $post;
    }

    public function save(BlogInterface $post)
    {
        $post->getResource()->save($post);
        return $post;
    }

    public function delete(BlogInterface $post)
    {
        $post->getResource()->delete($post);
    }



    public function getList(SearchCriteriaInterface $searchCriteria)
    {

        // todo check  \Magento\Eav\Model\AttributeSetRepository

        $collection = $this->blogCollectionFactory->create();
        $collection->load();

        return $this->buildSearchResult($searchCriteria, $collection);
    }

}
