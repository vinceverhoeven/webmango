<?php
namespace Webmango\WebmangoBlog\Model\Post;

use Webmango\WebmangoBlog\Model\Post\ResourceModel\Grid\CollectionFactory;
use Webmango\WebmangoBlog\Model\Post\BlogFactory;
use Webmango\WebmangoBlog\Controller\Adminhtml\Image\Upload as WebmangoBlogUploader;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    protected $uploader;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $blogCollectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $blogCollectionFactory,
        WebmangoBlogUploader $uploader,
        array $meta = [],
        array $data = []
    ) {
        $this->uploader = $uploader;
        $this->collection = $blogCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();
        $this->loadedData = array();

        foreach ($items as $blog) {
            $itemData = $blog->getData();
            if (isset($itemData['thumbnail'])) {
                $itemData['thumbnail'] = [
                    [
                        'name' => $itemData['thumbnail'],
                        'url' =>  $this->uploader->getMediaDir() . $itemData['thumbnail'],
                    ],
                ];
            } else {
                $itemData['thumbnail'] = null;
            }
            $this->loadedData[$blog->getId()] = $itemData;
        }
        return $this->loadedData;
    }
}
