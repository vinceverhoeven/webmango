<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Webmango\WebmangoBlog\Block\Adminhtml\Post\Edit;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Webmango\WebmangoBlog\Model\Post\BlogFactory;

/**
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var BlogFactory
     */
    protected $blogFactory;

    /**
     * @param Context $context
     * @param BlogFactory $blogFactory
     */
    public function __construct(
        Context $context,
        BlogFactory $blogFactory
    ) {
        $this->context = $context;
        $this->blogFactory = $blogFactory;
    }

    /**
     * Return CMS page ID
     *
     * @return int|null
     */
    public function getBlogId()
    {
        try {
            return $this->blogFactory->create()->load(
                $this->context->getRequest()->getParam('blog_id')
            )->getId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
