<?php

namespace Webmango\WebmangoBlog\Block;

use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Webmango\WebmangoBlog\Model\Post\ResourceModel\Grid\CollectionFactory;
use Webmango\WebmangoBlog\Controller\Adminhtml\Image\Upload as WebmangoBlogUploader;


class Blog extends Template
{

    const NEWS_LATEST_LIMIT = 3;

    protected $_blogCollectionFactory;

    protected $_imageUpload;

    public function __construct(
        WebmangoBlogUploader $imageUpload,
        Context $context,
        CollectionFactory $blogCollectionFactory,
        array $data = []
    )
    {
        $this->_imageUpload = $imageUpload;
        $this->_blogCollectionFactory = $blogCollectionFactory->create();
        parent::__construct($context, $data);
    }

    public function getLatestBlogs($limit = self::NEWS_LATEST_LIMIT)
    {
        $BlogCollection = $this->_blogCollectionFactory->setPageSize(3)->getItems();
        return $BlogCollection;
    }

    public function getThumbnailImageSrc($imgName)
    {
        return $this->_imageUpload->getMediaDir() . $imgName;
    }

    public function getFullUrl($urlKey)
    {

    }

    public function truncateText($string, $limit)
    {
        return mb_strimwidth($string, 0, $limit, "...");
    }

}
