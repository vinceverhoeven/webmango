<?php
namespace Webmango\WebmangoBlog\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use \Webmango\WebmangoBlog\Api\Data\BlogInterface;

interface BlogRepositoryInterface
{
    /**
     * @param int $id
     * @return \Webmango\WebmangoBlog\Api\Data\BlogInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($id);

    /**
     * @param \Webmango\WebmangoBlog\Api\Data\BlogInterface $post
     * @return \Webmango\WebmangoBlog\Api\Data\BlogInterface
     */
    public function save(BlogInterface $post);

    /**
     * @param \Webmango\WebmangoBlog\Api\Data\BlogInterface $post
     * @return void
     */
    public function delete(BlogInterface $post);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Webmango\WebmangoBlog\Api\Data\BlogSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}
