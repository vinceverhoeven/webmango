<?php
namespace Webmango\WebmangoBlog\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface BlogSearchResultInterface extends SearchResultsInterface
{
    /**
     * @return \Webmango\WebmangoBlog\Api\Data\BlogInterface[]
     */
    public function getItems();

    /**
     * @param \Webmango\WebmangoBlog\Api\Data\BlogInterface[] $items
     * @return void
     */
    public function setItems(array $items);
}
