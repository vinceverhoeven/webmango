<?php
namespace Webmango\WebmangoBlog\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface BlogInterface extends ExtensibleDataInterface
{
    /**
     * @return int
     */
    public function getBlogId();

    /**
     * @param int $id
     * @return void
     */
    public function setBlogId($id);

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param string $title
     * @return void
     */
    public function setTitle($title);

    /**
     * @return \Webmango\WebmangoBlog\Api\Data\BlogExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * @param \Webmango\WebmangoBlog\Api\Data\BlogExtensionInterface $extensionAttributes
     * @return void
     */
    public function setExtensionAttributes(BlogExtensionInterface $extensionAttributes);
}
