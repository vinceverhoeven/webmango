<?php

namespace Webmango\CatalogExtend\Model\Product;

use Magento\Catalog\Api\ProductRepositoryInterface as ProductRepository;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Api\SearchCriteriaBuilder;

class Product
{
    protected $filterGroup;

    protected $sortOrder;

    protected $searchCriteria;

    protected $productRepository;

    public function __construct(
        ProductRepository $productRepository,
        FilterGroup $filterGroup,
        SortOrder $sortOrder,
        SearchCriteriaBuilder $searchCriteria
    )
    {
        $this->filterGroup = $filterGroup;
        $this->searchCriteria = $searchCriteria;
        $this->sortOrder = $sortOrder;
        $this->productRepository = $productRepository;
    }

    public function getLatestProducts($limit = 6)
    {
        $this->sortOrder->setField('updated_at');
        $this->sortOrder->setDirection(SortOrder::SORT_DESC);

        $this->searchCriteria->setFilterGroups([$this->filterGroup]);
        $this->searchCriteria->setPageSize($limit);
        $this->searchCriteria->setSortOrders([$this->sortOrder]);

        $searchCriteria = $this->searchCriteria->create();
        $productList = $this->productRepository->getList($searchCriteria);

        return $productList->getItems();
    }


    public function getCountProducts()
    {
        $searchCriteria = $this->searchCriteria->create();
        $productList = $this->productRepository->getList($searchCriteria);

        return $productList->getTotalCount();
    }
}
