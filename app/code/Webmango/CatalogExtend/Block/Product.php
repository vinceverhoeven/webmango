<?php

namespace Webmango\CatalogExtend\Block;

use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use Webmango\CatalogExtend\Model\Product\Product as ProductModel;

class Product extends Template
{

    protected $productModel;

    public function __construct(
        ProductModel $productModel,
        Context $context
    )
    {
        $this->productModel = $productModel;
        parent::__construct($context);
    }

    public function productBlockGetLatestProducts($limit = 6)
    {
        return $this->productModel->getLatestProducts($limit);
    }


    public function productBlockGetCountProducts()
    {
        return $this->productModel->getCountProducts();
    }


}
