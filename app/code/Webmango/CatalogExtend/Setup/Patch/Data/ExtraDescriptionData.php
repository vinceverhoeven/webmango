<?php

namespace Webmango\CatalogExtend\Setup\Patch\Data;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class ExtraDescriptionData implements DataPatchInterface
{
    /** @var ModuleDataSetupInterface */
    private $moduleDataSetup;

    /** @var EavSetupFactory */
    private $eavSetupFactory;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $eavSetup->addAttributeGroup(
            \Magento\Catalog\Model\Product::ENTITY,
            'Default',
            'Extra content',
            20);

        $eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY,
            'extra_description', [
                'type' => 'text',
                'label' => 'Extra Description',
                'input' => 'textarea',
                'required' => false,
                'wysiwyg_enabled' => true,
                'group' => 'Extra content',
                'visible' => true,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'used_for_sort_by'=> false,
                'filterable'=> false,
                'filterable_in_search'=> false,
                'used_in_grid'=> false,
                'visible_in_grid'=> false,
                'comparable'=> false,
                'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
