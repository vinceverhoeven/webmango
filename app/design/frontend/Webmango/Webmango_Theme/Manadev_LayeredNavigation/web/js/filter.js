define(['ko', 'jquery'], function (ko, $) {
    return function (config) {
        var self = this;

        this.mobile = true;
        this.mobilePoint = 768;

        this.icon = ko.observable("fas fa-filter");
        this.highlight = ko.observable(true);

        this.clickMe = function () {
            self.highlight(!self.highlight());
            if (!self.highlight()) {
                self.icon("fas fa-times");
                $(config.filterBlock).addClass('active');
            } else {
                self.icon("fas fa-filter");
                $(config.filterBlock).removeClass('active');
            }
        }

        $(window).resize(function () {
            moveSidebar();
        });

        if($(window).width()  <= self.mobilePoint){
            self.mobile = false;
            moveSidebar();
        }else{
            self.mobile = true;
            moveSidebar();
        }

        function moveSidebar(){
            var windowWidth = $(window).width();

            if (windowWidth <= self.mobilePoint && !self.mobile) {
                console.log("mobile");
                $(config.filterWrapper).prependTo("#maincontent");
                self.mobile = true;
            }else if(windowWidth > self.mobilePoint && self.mobile){
                $(config.filterWrapper).prependTo(".sidebar.sidebar-main");
                self.mobile = false;
                console.log("desktop");
            }
        }
    }
});
