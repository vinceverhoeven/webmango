define([
    'jquery',
    'domReady!',
], function ($) {

    function init(dom) {
        $(window).on('resize',function () {
            moveCart(dom);
        });
    }

    function moveCart(dom) {
        if ($(window).width() > 768) {
            $(dom).appendTo('.sections.nav-sections');
        } else {
            $(dom).appendTo('.header-col-left .header.content');
        }
    }

    return function (config, dom) {
        init(dom);
        moveCart(dom);
    }
});
